package com.sprysa;



import java.util.Scanner;

public class Main {

  private void interval(int begin, int end) {
    int sum = 0;
    System.out.print("Odd numbers from interval (from begin to end): ");
    for (int i = begin; i <= end; i++) {
      if (i % 2 == 1) {
        System.out.print(i + " ");
        sum += i;
      }
    }
    System.out.print("\n" + "Sum of odd numbers from interval: " + sum);
    sum = 0;
    System.out.print("\n" + "Even numbers from interval (from end to begin): ");
    for (int i = end; i >= begin; i--) {
      if (i % 2 == 0) {
        System.out.print(i + " ");
        sum += i;
      }
    }
    System.out.print("\n" + "Sum of even numbers from interval: " + sum);
  }

  private void fibonacci(int sizeOfSet) {
    int[] fibonacci = new int[sizeOfSet];
    fibonacci[0] = 1;
    fibonacci[1] = 1;
    System.out.print("Fibonacci numbers: 1 1 ");
    for (int i = 2; i < sizeOfSet; i++) {
      fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
      System.out.print(fibonacci[i] + " ");
    }
    for (int i = sizeOfSet - 1; i >= 0; i--) {
      if (fibonacci[i] % 2 == 1) {
        System.out.print("\n" + "Biggest odd Fibonacci number: " + fibonacci[i]);
        break;
      }
    }
    for (int i = sizeOfSet - 1; i >= 0; i--) {
      if (fibonacci[i] % 2 == 0) {
        System.out.print("\n" + "Biggest even Fibonacci number: " + fibonacci[i]);
        break;
      }
    }
    int count = 0;
    for (int i = 0; i <= sizeOfSet - 1; i++) {
      if (fibonacci[i] % 2 == 1) {
        count++;
      }
    }
    System.out.print("\n" + "Percentage of odd numbers: " + (count * 100 / sizeOfSet) + "%");
    System.out.print("\n" + "Percentage of even numbers: " + (100 - count * 100 / sizeOfSet) + "%");
  }

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
      System.out.println("Enter interval parameters:");
      System.out.print("Begin of interval: ");
      int begin = Integer.parseInt(scanner.nextLine());
    try {
      if (begin<0){
        throw new Exception();
      }
    }catch(Exception b){
      System.out.println("Begin of interval must be >= 0. Please try again.");
      scanner.close();
      System.exit(0);
    }
      System.out.print("End of interval: ");
      int end = Integer.parseInt(scanner.nextLine());
      try{
        if(end<begin){
          throw new Exception();
        }
      }catch(Exception e){
        System.out.println("End of interval must be >= Begin of interval. Please try again.");
        scanner.close();
        System.exit(0);
      }
      new Main().interval(begin, end);
    System.out.print("\n" + "Enter the size of set of Fibonacci numbers: ");
    int sizeOfSet = Integer.parseInt(scanner.nextLine());
    try{
      if(sizeOfSet<2){
        throw new Exception();
      }
    }catch(Exception f){
      System.out.println("Size of set of Fibonacci numbers must be >= 2. Please try again.");
      scanner.close();
      System.exit(0);
    }
    new Main().fibonacci(sizeOfSet);
      scanner.close();
  }
}